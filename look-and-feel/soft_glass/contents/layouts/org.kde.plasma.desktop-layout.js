var plasma = getApiVersion(1);

var layout = {
    "desktops": [
        {
            "applets": [
            ],
            "config": {
                "/": {
                    "formfactor": "0",
                    "immutability": "1",
                    "lastScreen": "0",
                    "wallpaperplugin": "org.kde.image"
                },
                "/ConfigDialog": {
                    "DialogHeight": "684",
                    "DialogWidth": "1095"
                },
                "/General": {
                    "showToolbox": "false"
                },
                "/Wallpaper/General": {
                    "Color": "8,8,8",
                    "FillMode": "2",
                    "Image": "",
                    "SlideInterval": "1",
                    "SlidePaths": "\\0"
                },
                "/Wallpaper/org.kde.color/General": {
                    "Color": "8,8,8"
                },
                "/Wallpaper/org.kde.image/General": {
                    "Color": "8,8,8",
                    "FillMode": "2",
                    "Image": "",
                    "SlideInterval": "1"
                }
            },
            "wallpaperPlugin": "org.kde.image"
        }
    ],
    "panels": [
        {
            "alignment": "center",
            "applets": [
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration/Configuration/General": {
                            "favoritesPortedToKAstats": "true",
                            "systemApplications": "systemsettings.desktop,org.kde.kinfocenter.desktop"
                        },
                        "/Configuration/Configuration/General": {
                            "favoritesPortedToKAstats": "true",
                            "systemApplications": ""
                        },
                        "/Configuration/Configuration/Shortcuts": {
                            "global": "Alt+F1"
                        },
                        "/Configuration/General": {
                            "favoritesPortedToKAstats": "true",
                            "systemApplications": ""
                        },
                        "/Configuration/Shortcuts": {
                            "global": "Alt+F1"
                        },
                        "/Shortcuts": {
                            "global": "Alt+F1"
                        }
                    },
                    "plugin": "org.kde.plasma.kickoff"
                },
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration/Configuration/General": {
                            "launchers": "applications:org.kde.dolphin.desktop,applications:org.kde.kate.desktop,applications:firefox.desktop,applications:cantata.desktop,applications:inkscape.desktop,applications:org.kde.kmail2.desktop,applications:org.kde.akregator.desktop,applications:org.kde.showfoto.desktop,applications:systemsettings.desktop,applications:org.kde.konsole.desktop,applications:audacious.desktop,applications:qbittorrent.desktop,applications:gimp.desktop"
                        },
                        "/Configuration/Configuration/General": {
                            "launchers": "applications:org.kde.dolphin.desktop,applications:firefox.desktop,applications:systemsettings.desktop,applications:org.kde.konsole.desktop,applications:gimp.desktop,applications:inkscape.desktop,applications:org.kde.okular.desktop,applications:audacious.desktop,applications:vokoscreen.desktop,applications:kodi.desktop,applications:mpv.desktop,applications:org.kde.kmail2.desktop,applications:org.kde.akregator.desktop,applications:org.kde.kate.desktop,applications:qbittorrent.desktop"
                        },
                        "/Configuration/General": {
                            "launchers": "applications:firefox.desktop,applications:org.kde.dolphin.desktop,applications:org.kde.kate.desktop"
                        }
                    },
                    "plugin": "org.kde.plasma.icontasks"
                },
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        }
                    },
                    "plugin": "org.kde.plasma.panelspacer"
                }
            ],
            "config": {
                "/": {
                    "formfactor": "3",
                    "immutability": "1",
                    "lastScreen": "0",
                    "wallpaperplugin": "org.kde.image"
                },
                "/ConfigDialog": {
                    "DialogHeight": "1080",
                    "DialogWidth": "166"
                }
            },
            "height": 2,
            "hiding": "normal",
            "location": "left",
            "maximumLength": 60,
            "minimumLength": 60,
            "offset": 0
        },
        {
            "alignment": "right",
            "applets": [
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration/Configuration/General": {
                            "length": "231"
                        },
                        "/Configuration/Configuration/General": {
                            "length": "231"
                        },
                        "/Configuration/General": {
                            "length": "231"
                        }
                    },
                    "plugin": "org.kde.plasma.panelspacer"
                },
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration": {
                            "immutability": "1"
                        }
                    },
                    "plugin": "org.kde.plasma.systemtray"
                },
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Appearance": {
                            "enabledCalendarPlugins": "/opt/kf5/lib/plugins/plasmacalendarplugins/holidaysevents.so\\,/opt/kf5/lib/plugins/plasmacalendarplugins/pimevents.so"
                        },
                        "/Configuration/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration/Appearance": {
                            "enabledCalendarPlugins": "/opt/kf5/lib/plugins/plasmacalendarplugins/holidaysevents.so\\,/opt/kf5/lib/plugins/plasmacalendarplugins/pimevents.so"
                        },
                        "/Configuration/Configuration/Configuration/Appearance": {
                            "enabledCalendarPlugins": "/opt/kf5/lib/plugins/plasmacalendarplugins/holidaysevents.so,/opt/kf5/lib/plugins/plasmacalendarplugins/pimevents.so"
                        },
                        "/Configuration/Configuration/Configuration/ConfigDialog": {
                            "DialogHeight": "540",
                            "DialogWidth": "720"
                        }
                    },
                    "plugin": "org.kde.plasma.digitalclock"
                }
            ],
            "config": {
                "/": {
                    "formfactor": "2",
                    "immutability": "1",
                    "lastScreen": "0",
                    "wallpaperplugin": "org.kde.image"
                },
                "/ConfigDialog": {
                    "DialogHeight": "82",
                    "DialogWidth": "1920"
                }
            },
            "height": 1.6666666666666667,
            "hiding": "windowsbelow",
            "location": "top",
            "maximumLength": 15.777777777777779,
            "minimumLength": 15.444444444444445,
            "offset": 0
        },
        {
            "alignment": "right",
            "applets": [
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1"
                        },
                        "/Configuration/Configuration/General": {
                            "RecentSources1": "[{\"icon\":\"cantata\",\"identity\":\"Cantata\",\"source\":\"cantata\"}]"
                        },
                        "/Configuration/General": {
                            "RecentSources1": "[{\"icon\":\"cantata\",\"identity\":\"Cantata\",\"source\":\"cantata\"}]"
                        }
                    },
                    "plugin": "audoban.applet.playbar"
                },
                {
                    "config": {
                        "/": {
                            "immutability": "1"
                        },
                        "/Configuration": {
                            "immutability": "1",
                            "source": "wettercom|weather|Melbourne, State of Victoria, AU|AU0VI0027;Melbourne",
                            "updateInterval": "30",
                            "weatherServiceProviders": "bbcukmet,wettercom"
                        },
                        "/Configuration/ConfigDialog": {
                            "DialogHeight": "540",
                            "DialogWidth": "720"
                        },
                        "/Configuration/Configuration": {
                            "source": "wettercom|weather|Melbourne, State of Victoria, AU|AU0VI0027;Melbourne",
                            "updateInterval": "30",
                            "weatherServiceProviders": "wettercom"
                        },
                        "/Configuration/Configuration/ConfigDialog": {
                            "DialogHeight": "540",
                            "DialogWidth": "720"
                        }
                    },
                    "plugin": "org.kde.plasma.weather"
                }
            ],
            "config": {
                "/": {
                    "formfactor": "2",
                    "immutability": "1",
                    "lastScreen": "0",
                    "wallpaperplugin": "org.kde.image"
                },
                "/ConfigDialog": {
                    "DialogHeight": "82",
                    "DialogWidth": "1920"
                }
            },
            "height": 1.8888888888888888,
            "hiding": "normal",
            "location": "bottom",
            "maximumLength": 88.72222222222223,
            "minimumLength": 23.555555555555557,
            "offset": 0
        }
    ],
    "serializationFormatVersion": "1"
}
;

plasma.loadSerializedLayout(layout);
